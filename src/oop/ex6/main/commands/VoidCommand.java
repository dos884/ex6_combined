package oop.ex6.main.commands;

import oop.ex6.main.ast_validator.BackValidationStrategy;
import oop.ex6.main.ast_validator.VoidStrategy;

/**
 * an empty command like newline or comment
 */
public class VoidCommand extends Command{
    @Override
    BackValidationStrategy getStrategy() {
        return VoidStrategy.getInst();
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public commandTypes getCommandType() {
        return commandTypes.VOID_COMMAND;
    }
}
