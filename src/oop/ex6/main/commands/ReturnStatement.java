package oop.ex6.main.commands;

import oop.ex6.main.ast_validator.BackValidationStrategy;
import oop.ex6.main.ast_validator.ReturnStrategy;

/**
 * a return statement
 */
public class ReturnStatement extends Command {

    /**
     * construc new returns statemnt
     */
    public ReturnStatement() {
        super();
    }

    @Override
    BackValidationStrategy getStrategy() {
        return ReturnStrategy.getInst();
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public commandTypes getCommandType() {
        return commandTypes.RETURN;
    }


}
