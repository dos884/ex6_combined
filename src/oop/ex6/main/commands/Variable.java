package oop.ex6.main.commands;

/**
 * a variable
 */
public class Variable {


    final valueTypes type;
    final boolean isFinal;
    boolean isInitialized;
    final private String name;


    /**
     * constructor
     * @param isFinal
     * @param type
     * @param isInitialized
     * @param name
     */
    public Variable(boolean isFinal, valueTypes type, boolean isInitialized, String name) {
        this.type = type;
        this.isFinal = isFinal;
        this.isInitialized = isInitialized;
        this.name = name;
    }

    /**
     * copy constructor
     * @param toCopy
     */
    public Variable(Variable toCopy){
        this.type = toCopy.type;
        this.isFinal = toCopy.isFinal;
        this.isInitialized = toCopy.isInitialized;
        this.name = toCopy.name;
    }

    public Variable(valueTypes type, String name){
        this(false,type,true,name);
    }


    /**
     * Create Variable from Literal type. we dont care about value!
     * @param type - ...
     */
    public Variable(valueTypes type){
        this(false,type,true,"#LITERAL");

    }

    @Override
    public String toString() {
        return  "<"+(isFinal?"final ": "") + type + ", " + name + ", " + (isInitialized ? "i":"n_i")+">";
    }

    /**
     * -
     * @return name of var
     */
    public String getName() {
        return name;
    }

    /**
     * set intitialization state
     * @param isInitialized
     */
    public void setInitialized(boolean isInitialized) {
        this.isInitialized = isInitialized;
    }

    /**
     * -
     * @return is the variable initialized
     */
    public boolean isInitialized() {
        return isInitialized;
    }

    /**
     * does this var type matches other input type
     * mainly used to check if assignment into thisVar is legal from other var.
     * @param thisVar the lhs var
     * @param other the rhs argument
     * @return do values mathch?
     */
    public static boolean isTypeMatch(Variable thisVar, Variable other){
        boolean flag = false;
        if (thisVar.type == valueTypes.BOOL){
            //todo what are the boll specs?
            return other.type == valueTypes.DOUBLE ||
                    other.type == valueTypes.INT ||
                    other.type == valueTypes.BOOL ;
        } if (thisVar.type == valueTypes.INT){
            return other.type == valueTypes.INT;
        } if (thisVar.type == valueTypes.DOUBLE) {
            flag = other.type== valueTypes.INT ||
                    other.type== valueTypes.DOUBLE;
            return flag;
        } if (thisVar.type == valueTypes.STRING){
            flag = other.type== valueTypes.STRING;
            return flag;
        } if (thisVar.type == valueTypes.CHAR){
            return other.type== valueTypes.CHAR;
        }
        return false;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public static enum valueTypes {
        INT, STRING, CHAR, DOUBLE, BOOL, UNKNOWN
    }
}
