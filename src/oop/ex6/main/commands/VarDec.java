package oop.ex6.main.commands;

import oop.ex6.main.ast_validator.BackValidationStrategy;
import oop.ex6.main.ast_validator.VarDecStrategy;

/**
 * variable declaration statement
 */
public class VarDec extends Command {


    boolean isInFuncDec = false;
    private AssignmentArgument arg;
    private Variable var;

    /**
     * variable declaration statement constructor
     * @param isFinal
     * @param type
     * @param name
     * @param argument
     */
    public VarDec(boolean isFinal ,Variable.valueTypes type, String name, AssignmentArgument argument) {
        super();

        super.setName(name);
        boolean isInited = !(argument==null);
        arg = argument;
        var = new Variable(isFinal, type, isInited, name);

    }

    /**
     * constructor for declartion with no rhs argument.
     * @param isFinal
     * @param type
     * @param name
     */
    public VarDec(boolean isFinal ,Variable.valueTypes type, String name) {

        this(isFinal, type, name, null);

    }

    /**
     * initialize the var
     */
    public void initVar(){
        var.setInitialized(true);
    }

    /**
     * copy constructor
     * @param toCopy
     */
    public VarDec(Variable toCopy){
        super();
        var = new Variable(toCopy);

    }

    /**
     * -
     * @return gets the arguments
     */
    public AssignmentArgument getArg() {
        return arg;
    }

    @Override
    public commandTypes getCommandType() {
        return commandTypes.VAR_DEC;
    }


    /**
     * compares by name another command
     * @param other
     * @return
     */
    public boolean compare(Command other){
        try {
            VarDec vd = (VarDec) other;

            if (vd.getName().equals(this.getName())){
                return true;
            }
        } catch (Exception ignored){

        }
        return false;
    }


    @Override
    BackValidationStrategy getStrategy() {
        return VarDecStrategy.getInst();
    }

    @Override
    public String toString() {
        return super.toString() + var.toString() + " " + arg;
    }

    @Override
    public String getName() {
        return var.getName();
    }

    /**
     * get the contained variable
     * @return
     */
    public Variable getVar() {
        return var;
    }
}
