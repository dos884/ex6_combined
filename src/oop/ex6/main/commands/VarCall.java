package oop.ex6.main.commands;

import oop.ex6.main.ast_validator.BackValidationStrategy;
import oop.ex6.main.ast_validator.VarAssignmentStrategy;

/**
 * var assignment statement
 */
public class VarCall extends Command {

    private final String name;
    private final AssignmentArgument arg;

    //todo varCAll can have getVar() coz he dont have no var! only argunment

    /**
     * construc a var assignment
     * @param name
     * @param arg
     */
    public VarCall(String name, AssignmentArgument arg) {

        super();
        this.name = name;
        this.arg = arg;
    }

    /**
     * gets the rhs of the assignment
     * @return rhs
     */
    public AssignmentArgument getRHSArg() {
        return arg;
    }

    /**
     * gets the lhs side of the assignment
     * @return lhs
     */
    public AssignmentArgument getLHSArg(){
        return new AssignmentArgument(name);
    }

    @Override
    BackValidationStrategy getStrategy() {
        return VarAssignmentStrategy.getInst();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public commandTypes getCommandType() {
        return commandTypes.VAR_CALL;
    }

    @Override
    public String toString() {
        return super.toString()+"VarCall: "  + getName() + " |-| " + getRHSArg();
    }
}
