package oop.ex6.main.commands;

import oop.ex6.main.ast_validator.BackValidationStrategy;
import oop.ex6.main.ast_validator.ConditionalStrategy;

import java.util.List;

/**
 * conditional command
 */
public class Cond extends Command {
    List<AssignmentArgument> args;

    public static final String CONDITION_KEYWORD = new String ("COND");

    /**
     * construct conditional statement with arguments
     * @param args
     */
    public Cond(List<AssignmentArgument> args) {
        super();
        this.args = args;
    }

    /**
     * -
     * @return gets a list a list of arguments we need to back-validate
     */
    public List<AssignmentArgument> getArgs(){
        return args;
    }


    @Override
    BackValidationStrategy getStrategy() {
        return ConditionalStrategy.getInst();
    }

    @Override
    public String getName() {
        return CONDITION_KEYWORD;
    }

    @Override
    public commandTypes getCommandType() {
        return commandTypes.COND;
    }

    @Override
    public String toString() {
        return super.toString() + args;
    }
}
