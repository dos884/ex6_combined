package oop.ex6.main.commands;

import oop.ex6.main.ast_validator.BackValidationStrategy;
import oop.ex6.main.ast_validator.FunctionDeclerationStrategy;

import java.util.Iterator;
import java.util.List;

/**
 * a general function declaration statement
 */
public class FuncDec extends FunctionCommand {

    public FuncDec(String name, List<Variable> vars) {
        super(name, vars);
        this.vars = vars;
    }

    public boolean matchVars(List<Variable> varsToMatch){
        if (vars.size()==varsToMatch.size()) {
            Iterator<Variable> thisIterator = vars.iterator();
            Iterator<Variable> otherIterator = varsToMatch.iterator();
            while (thisIterator.hasNext() && otherIterator.hasNext()) {
                if (!Variable.isTypeMatch(thisIterator.next(),otherIterator.next())) {
//                    throw new ValidationException()
                    return false;
                }
            }
        }
        return true;
    }


    @Override
    BackValidationStrategy getStrategy() {
        return FunctionDeclerationStrategy.getInst();
    }

    @Override
    public commandTypes getCommandType() {
        return commandTypes.FUNC_DEC;
    }

    @Override
    public String toString() {
        return super.toString() + name + vars;
    }
}
