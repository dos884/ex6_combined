package oop.ex6.main.commands;

/**
 * exception at command creation
 */
public class CommandCreationException extends RuntimeException {

    public CommandCreationException() {
        super();
    }

    public CommandCreationException(String message) {
        super(message);
    }
}
