package oop.ex6.main.commands;

import java.util.List;

/**
 * a general function statement
 */
public abstract class FunctionCommand extends Command {
    List<Variable> vars;
    String name="";

    public FunctionCommand(String name, List<Variable> vars) {
        super();
        this.vars = vars;
        this.name = name;
    }



    public List<Variable> getVars() {
        return vars;
    }

    @Override
    public String toString() {
        String out = "";
        for (Variable v : vars){
            out = out + v.toString() +" | ";
        }
        return super.toString() + out;
    }

    @Override
    public String getName() {
        return name;
    }
}
