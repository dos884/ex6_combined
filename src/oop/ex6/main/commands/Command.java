package oop.ex6.main.commands;


import oop.ex6.main.ast.ASTCommandNode;
import oop.ex6.main.ast_validator.BackValidationStrategy;

/**
 * An abstract statment in sjava
 */
public abstract class Command {


    public static int lineNum = 0;
    private int instLineNum;

    private boolean isNamed = false;
    private String name;


    Command(){
        /*
            counts the line of the command. used mainly for debuging.
         */
        lineNum++;
        instLineNum = Command.lineNum;
    }


    /**
     *
     * @param name set the name of the command
     */
    public void setName(String name) {
//        this();
        if (!isNamed) {
            this.name = name;
        } else {
            //todo
            throw new RuntimeException("name");
        }
    }

    /**
     * this gets the strategy each subclass must returns
     * @return a strategy object
     */
    abstract BackValidationStrategy getStrategy();

    /**
     * the types of the commands
     */
    public static enum commandTypes {
        FUNC_DEC, FUNC_CALL, VAR_DEC, VAR_CALL, COND, EOS, RETURN, VOID_COMMAND, FUNC_VAR_DEC
    }

    @Override
    public String toString() {
        return instLineNum + ". " +this.getClass().toString().split("[ .]")[5] + ": ";
    }

    /**
     * get the name of this command
     * @return
     */
    public abstract String getName();

    /**
     * -
     * @return - type of this command
     */
    public abstract commandTypes getCommandType();

    /**
     * this is the function the validator calls when iterating the tree
     * @param node
     * @param global
     */
    public void backValidate(ASTCommandNode node, boolean global){
        getStrategy().invoke(node,global);
    }

    /**
     * -
     * @return get the associated var.
     */
    public Variable getVar() {
        return null;
    }

}
