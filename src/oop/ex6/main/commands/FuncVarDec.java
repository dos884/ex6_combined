package oop.ex6.main.commands;

/**
 * a var declaration inside of a function declaration
 * requires different handling than a normal var-dec
 */
public class FuncVarDec extends VarDec {
    public FuncVarDec(boolean isFinal, Variable.valueTypes type, String name, AssignmentArgument argument) {
        super(isFinal, type, name, argument);
        getVar().setInitialized(true);
        isInFuncDec = true;
    }

    public FuncVarDec(boolean isFinal, Variable.valueTypes type, String name) {
        super(isFinal, type, name);
        getVar().setInitialized(true);
    }

    public FuncVarDec(Variable toCopy) {
        super(toCopy);
        getVar().setInitialized(true);

    }

    @Override
    public commandTypes getCommandType() {
        return commandTypes.FUNC_VAR_DEC;
    }
}
