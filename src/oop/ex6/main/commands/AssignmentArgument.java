package oop.ex6.main.commands;

/**
 * an argument class. can be a literal or a name of a variable.
 */
public class AssignmentArgument {

    private boolean isLiteral;
    private String name;
    private String value;
    private Variable.valueTypes type;
    private static final String ERROR = "ERROR";

    private AssignmentArgument(boolean isLiteral, String arg, Variable.valueTypes type) {
        this.isLiteral = isLiteral;
        if (isLiteral){
            value=arg;
            name = ERROR;
            this.type = type;
        } else {
            name = arg;
            value = ERROR;
            this.type= Variable.valueTypes.UNKNOWN;
        }
    }

    /**
     * creates a new Variable-pointer
     * which will be search for
     * @param name name of the var
     */
    public AssignmentArgument(String name){
        this(false,name, Variable.valueTypes.UNKNOWN);
    }

    /**
     * creates a new literal pointer
     * @param literalValue value of the litera
     * @param type - type of the literal which should be infered at the syntax parsing stage.
     */
    public AssignmentArgument(String literalValue, Variable.valueTypes type){
        this(true, literalValue, type);
    }

    /**
     *
     * @return is it a literal
     */
    public  boolean isIsLiteral() {
        return isLiteral;
    }

    /**
     *
     * @return the name
     */
    public  String getName() {
        if (!isIsLiteral()) return name;
        else throw new CommandCreationException("literal isnt avar");
    }

    /**
     *
     * @return type of argument, or exception if it is typeless
     */
    public Variable.valueTypes getType() {
        if (isLiteral)return type;
        throw new CommandCreationException("hasno type");
    }


    @Override
    public String toString() {
        return "[arg: " + (isIsLiteral()?("#"+value+":" + type + "]"):("$"+name+"]"));
    }
}
