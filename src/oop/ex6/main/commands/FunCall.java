package oop.ex6.main.commands;

import oop.ex6.main.ast_validator.BackValidationStrategy;
import oop.ex6.main.ast_validator.FunctionCallStrategy;

import java.util.List;

/**
 * function call statement
 */
public class FunCall extends Command{

    List<AssignmentArgument> args;
    String name;

    /**
     * constructor.
     * @param name name of functrion
     * @param args argument list
     */
    public FunCall(String name, List<AssignmentArgument> args) {
        super();
        this.name = name;
        this.args = args;
    }

    /**
     * get arguments of function call
     * @return
     */
    public List<AssignmentArgument> getArgs() {
        return args;
    }

    @Override
    BackValidationStrategy getStrategy() {
        return FunctionCallStrategy.getInst();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public commandTypes getCommandType() {
        return commandTypes.FUNC_CALL;
    }

    @Override
    public String toString() {
        return super.toString() +"{" +
                "args=" + args +
                ", name='" + name + '\'' +
                '}';
    }

}
