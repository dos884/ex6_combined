package oop.ex6.main.commands;

import oop.ex6.main.ast_validator.BackValidationStrategy;
import oop.ex6.main.ast_validator.EndScopeStrategy;

/**
 * scope end command
 */
public class EndScope extends Command {
    @Override
    BackValidationStrategy getStrategy() {
        return EndScopeStrategy.getInst();
    }

    @Override
    public String getName() {
        return "EOS";
    }

    @Override
    public commandTypes getCommandType() {
        return commandTypes.EOS;
    }
}
