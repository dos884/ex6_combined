package oop.ex6.main.ast;

import oop.ex6.main.commands.Command;

/**
 * a node representing a scope (a tree fork)
 */
public class ASTFork extends AbstractASTNode {

    private final ASTBuilder.forkTypes forkType;
    private String forkName;

    public ASTFork(ASTBuilder.forkTypes forkType, String forkName) {
        super();
        this.forkType = forkType;
        this.forkName = forkName;
        this.isLeaf = false;
    }

    /**
     * -
     * @return type of this fork. see enum {FUNCTION, CONDITIONAL, SIMPLE_SCOPE, GLOBAL} in ASTBuilder
     */
    public ASTBuilder.forkTypes getForkType() {
        return forkType;
    }

    @Override
    Command getCommand() {
        throw new UnsupportedOperationException("A fork doesnt have a command");
    }

    @Override
    public String toString() {
        return forkType.toString() + " " + forkName;
    }
}
