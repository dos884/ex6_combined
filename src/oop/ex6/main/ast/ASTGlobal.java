package oop.ex6.main.ast;

import java.util.HashSet;
import java.util.Set;

/**
 * a global scope
 */
public class ASTGlobal extends ASTFork {

    Set<AbstractASTNode> globalsChildrenSet;

    public ASTGlobal() {
        super(ASTBuilder.forkTypes.GLOBAL, "global");
        globalsChildrenSet = new HashSet<AbstractASTNode>();
    }

    @Override
    public AbstractASTNode addChild(AbstractASTNode child) {
        globalsChildrenSet.add(child);
        return super.addChild(child);
    }

    /**
     * get a ref to the global commands
     * @return
     */
    public Set<AbstractASTNode> getChildrenSet(){
        return globalsChildrenSet;
    }

    @Override
    public String toString() {
        return getForkType().toString();
    }
}
