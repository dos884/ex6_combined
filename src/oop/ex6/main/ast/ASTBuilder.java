package oop.ex6.main.ast;

import oop.ex6.main.utils.Logger;
import oop.ex6.main.commands.*;

/**
 * Created by dos884 on 6/11/15.
 */
public class ASTBuilder {

    private static ASTBuilder inst = new ASTBuilder();
    public static ASTBuilder getInst(){return inst;}
    private ASTBuilder(){}

    //bracket/scope balance counter
    private int scopeBalance = 0;

    public int lineNum = 1;

    public AbstractASTNode globalNode;
    public AbstractASTNode currentNode;

    private ASTHandler handler;

    boolean isInited = false;

    /**
     * the main ast building function. feeds new command to ast.
     * @param com - the command
     */
    public void newComamnd(Command com){
        Logger.printlnVV(lineNum + ". " + com + " " + currentNode);
        if (com.getCommandType()== Command.commandTypes.VOID_COMMAND){
            return;
        }

        lineNum++;

        if (com.getCommandType()== Command.commandTypes.FUNC_DEC){
            FuncDec funcDec = (FuncDec) com;
            createFunctionNode(funcDec);

        } else if (com.getCommandType()== Command.commandTypes.COND){
            newScope(forkTypes.CONDITIONAL,com.getName());
            currentNode.addChild(new ASTCommandNode(com));
        }

        else {
            currentNode.addChild(new ASTCommandNode(com));
        }
        if (com.getCommandType()== Command.commandTypes.EOS){
            exitScope();
            return;
        }
    }

    private void createFunctionNode(FuncDec funcDec) {
        if(currentNode==globalNode){
            //create func scope:
            newScope(forkTypes.FUNCTION ,"-"+funcDec.getName());
            for (Variable param : funcDec.getVars()) {
                //todo beware inf recursion
                //translate the parameter into a varDec in the new function scope
                newComamnd(new FuncVarDec(param));
            }
            handler.pushFuncDec(funcDec);
            // create func does not close the opened scope!
            // beacuse and explicit '}' is expected
        } else {
            throw new ASTBuildException("func dec not in global scope!");
        }
    }

    private void newScope(forkTypes type, String name){

        scopeBalance++;
        currentNode = currentNode.addChild(new ASTFork(type, name));
    }

    private void exitScope(){

        //todo check thet return statement is last command in theis scope

        scopeBalance--;
        if (scopeBalance<0){
            throw new ASTBuildException("Unbalanced Scopes! " + scopeBalance);
        }
//        if (currentNode.getParent() == null){
//            throw new ASTBuildException("tried to exit an outer scope");
         else {

            currentNode = currentNode.getParent();
        }
    }


    private void newGlobalScope(){

        globalNode = new ASTGlobal();
        handler = new ASTHandler((ASTGlobal)globalNode);
        currentNode = globalNode;
    }

    /**
     * initialize the ast builder
     */
    public void initBuilder(){
        newGlobalScope();
        scopeBalance = 0;
        lineNum =1;
    }


    /**
     * finalize and return the constructed ast handler
     * @return ast handler
     */
    public ASTHandler generateAST(){
        if (scopeBalance!=0){
            throw new ASTBuildException("Scope is unbalanced at end of creation: " + scopeBalance);
        }
        return handler;
    }

    /**
     * types of scopes
     */
    public static enum forkTypes{
        FUNCTION, CONDITIONAL, SIMPLE_SCOPE, GLOBAL
    }
}
