package oop.ex6.main.ast;

import oop.ex6.main.commands.Command;
import oop.ex6.main.commands.FuncDec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * holds the tree and some othe helpful data and functions
 */
public class ASTHandler {

    private final AbstractASTNode globalNode;

    HashMap<String, FuncDec> funcDecs;

    List<Command> globCommands;

    ASTHandler(ASTGlobal glob){
        globalNode = glob;
        funcDecs = new HashMap<String, FuncDec>();

        globCommands = new ArrayList<Command>();
    }

    /**
     * get the global scope
     * @return
     */
    public AbstractASTNode getGlobalNode() {
        return globalNode;
    }


    /**
     * push a new function declaration to the funcdec set.
     * @param dec
     */
    void pushFuncDec(FuncDec dec){
        funcDecs.put(dec.getName(),dec);
    }

    /**
     * print the tree
     */
    public void print(){
        globalNode.print();
    }

    /**
     * find a function declaration matching a given name
     * @param name
     * @return - function or null.
     */
    public FuncDec findFuncDec(String name){
        if (funcDecs.containsKey(name)){
            return funcDecs.get(name);
        } else return null;
    }
}
