package oop.ex6.main.ast;

import oop.ex6.main.commands.Command;

import java.util.List;

/**
 * A node object in the tree with a command contained in it.
 *
 */
public class ASTCommandNode extends AbstractASTNode {

    Command command;

    //    public void

    public ASTCommandNode(Command command) {
        this.command = command;
        this.isLeaf = true;
    }

    @Override
    public int getNumChildren() {
        return 0;
    }

    @Override
    AbstractASTNode getIthChild(int i) {
        throw new UnsupportedOperationException("Command nodes can't have Children!");
    }

    @Override
    public List<AbstractASTNode> getChildren() {
        throw new UnsupportedOperationException("Command nodes can't have Children!");
    }

    //    @Override
//    AbstractASTNode next(int currentPointer)


    @Override
    public AbstractASTNode addChild(AbstractASTNode child) {
        throw new UnsupportedOperationException("Command nodes can't have Children!");
    }

    @Override
    public Command getCommand() {
        return command;
    }

    @Override
    public String toString() {
        return command.toString();
    }
}
