package oop.ex6.main.ast;

import oop.ex6.main.utils.Logger;
import oop.ex6.main.commands.Command;

import java.util.ArrayList;
import java.util.List;

/**
 * an abstract tree node
 */
public abstract class AbstractASTNode {

    List<AbstractASTNode> children;
    //members

    private AbstractASTNode parent = null;
    private int height;
    int childNumber;
    boolean isLeaf;
    boolean isRoot;


    AbstractASTNode(){
        children = new ArrayList<AbstractASTNode>();
    }

    public static AbstractASTNode getFirstCommandNode(AbstractASTNode astFork){
        AbstractASTNode node = astFork;
        while (node.getNumChildren()>0) {
            node = node.getIthChild(0);
        }

            return node;
    }


    public AbstractASTNode addChild(AbstractASTNode child){
        child.setParent(this);
        // set the number of the child (will be used for traversal)
        child.childNumber = children.size();
        children.add(child);
        return child;
    }

    /**
     * iterative in-order traversal. (stoping height make no sense here)
     * @return the next node (in order of the lines)
     */
    public AbstractASTNode next(){
        //if no parent, then this must be root ==> we are at the last LOC then there is no next
        if (this.getParent()==null) return null;

        //if not end return next child
        if (childNumber<getParent().getNumChildren()-1){
            AbstractASTNode nextChild = getParent().getChildren().get(childNumber+1);
            if (nextChild.isLeaf){
                return nextChild;
            } else {
                return getFirstCommandNode(nextChild);
            }
        }
        //, need to call
        //parents next
        return getParent().next();
    }


    /**
     * back iterate until a certain depth level reached
     * @param stopAtDepth dont return nodes <= this depth
     * @return previous node
     */
    public AbstractASTNode prev(int stopAtDepth) {
        //if we are at the first LOC / reached end of the desired depth then we stop
        if (getParent()==null || getDepth()==stopAtDepth) return null;

        //if not first return previous
        if (this.childNumber>0){
            AbstractASTNode prevChild = getParent().getChildren().get(childNumber-1);
            if (prevChild.isLeaf) return prevChild;
            else return prevChild.prev(stopAtDepth);
        }
        else{
            return getParent().prev(stopAtDepth);
        }
    }
    AbstractASTNode getIthChild(int i){
        return children.get(i);
    }
//    abstract boolean checkConditionB()


    /*
            sets the parent. will throw NPE.
         */
    private void setParent(AbstractASTNode parent) {
        this.parent = parent;

    }

    /**
     * -
     * @return parent
     */
    public AbstractASTNode getParent() {
        return parent;
    }

    /**
     * Traverses up to the root to calculate depth.
     * it's O(log n) but it's better then updating the depths
     * of the whole tree when rotation occurs.
     * @return this nodes depth.
     */
    public int getDepth() {
        if (parent==null){return 0;}
        else return 1+parent.getDepth();

    }

    /**
     * -
     * @return key
     */
    abstract Command getCommand();

    /**
     * -
     * @return number of children
     */
    public int getNumChildren() {

        return children.size();
    }

    /**
     * -
     * @return a list of the children
     */
    public List<AbstractASTNode> getChildren(){
        return children;
    }

    /**
     * if we want only command leaves
     * @return leaf only nodes
     */
    public List<AbstractASTNode> getLeaves() {

        List<AbstractASTNode> leaves = new ArrayList<AbstractASTNode>();
        for (AbstractASTNode node : getChildren()){
            if (node.isLeaf){
                leaves.add(node);
            }
        }
        return leaves;

    }

    /**
     * prints the tree
     */
    public void print() {
        print("", true);
    }

    private void print(String prefix, boolean isTail) {
        Logger.println(prefix + (isTail ? "└── " : "├── ") + this);
        for (int i = 0; i < getChildrenForPrint().size() - 1; i++) {
            getChildrenForPrint().get(i).print(prefix + (isTail ? "    " : "│   "), false);
        }
        if (getChildrenForPrint().size() > 0) {
            getChildrenForPrint().get(getChildrenForPrint().size() - 1).print(prefix + (isTail ?"    " : "│   "), true);
        }
    }

    private List<AbstractASTNode> getChildrenForPrint(){
//        List<ASTNode> list = new ArrayList<ASTNode>();

        return children;
    }


}
