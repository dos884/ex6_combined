package oop.ex6.main.ast;

import oop.ex6.main.GeneralSjavaException;

/**
 * Created by dos884 on 6/12/15.
 */
public class ASTBuildException extends GeneralSjavaException {
    public ASTBuildException(String s) {
        super(s);
    }
}
