package oop.ex6.main.regexCommands;

/**
 * A class contains regex of simple lines such as: return, scope closing, comment and empty line.
 * also the corresponding methods return those regex
 */

public abstract class SimpleLineReg {
	
	private static final String RETURN = "return" + BasReg.MAYBE_SINGLE_SPACE + ";";
	private static final String END_SCOPE = "}";
	private static final String COMMENT = "//"+BasReg.ALL_CHARS_INC_SPACE ;
	private static final String EMPTY_LINE = "";

    /**
     * @return the regex which return line should fit to
     */
	public static String getReturnRegex(){
		return RETURN;
	}
    /**
     * @return the regex which ending scope line should fit to
     */
    public static String getEndScopeRegex(){
		return END_SCOPE;
	}
    /**
     * @return the regex which comment line should fit to
     */
    public static String getCommenRegex(){
        return COMMENT;
    }
    /**
     * @return the regex which empty line regex should fit to
     */
    public static String getEmptyLineRegex(){
        return EMPTY_LINE;
    }

}
