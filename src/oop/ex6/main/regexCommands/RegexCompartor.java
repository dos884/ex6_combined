package oop.ex6.main.regexCommands;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * A class used in order to compare between a given line and its correspond regex expression
 */
public class RegexCompartor {
	
	public static boolean ValidateLine (String line, String regex){

		Pattern p = Pattern.compile(regex);	
		Matcher m = p.matcher(line);
		return (m.matches());
	}
}
