package oop.ex6.main.regexCommands;

/**
 * A class contains conditional regex expressions and method, in order to determine if condition expression is right
 */

public class ConditionReg {
	// Condition specific signs 
	private static final String IF = "if", WHILE = "while";

	// Declaration structure

    //Var Or Literal!!
	private static final String SUB_CONDITION = "("+VariableReg.VARIABLE_NAME +"|" + VariableReg.ARG_NAME+ ")";

    private static final String DEC_START =  "(" + IF + "|" + WHILE + ")"+ BasReg.OPEN_PAR;
    private static final String DEC_MIDDLE_OPTION1 = SUB_CONDITION;
    private static final String DEC_MIDDLE_OPTION2 = "(" + "(" + BasReg.OR_CON + "|" + BasReg.AND_CON + ")" +
			SUB_CONDITION + ")" ;
    private static final String DEC_END = BasReg.CLOSE_PAR + BasReg.OPEN_BLOCK;

    //Added brackets!
    private static final String CON_DEC = DEC_START + "("+ DEC_MIDDLE_OPTION1 + DEC_MIDDLE_OPTION2 +
			BasReg.ZERO_OR_MORE + ")" + DEC_END;

    /**
     * @return the regex which condition line should fit to
     */
	public static String getConReg(){
		return CON_DEC;
	}
}
