package oop.ex6.main.regexCommands;

/**
 * A class contains method regex expressions and methods used in order to determine if method expression line such as
 * declaration or placement is right
 */

public class MethodReg {

	// Method specific signs 
	private static final String VOID = "void";

	// Declaration structure 
    private static final String SUB_DECLARATION = VariableReg.MAYBE_FINAL + VariableReg.VARIABLE_TYPE +
			BasReg.SINGLE_SPACE + VariableReg.VARIABLE_NAME;

    private static final String DEC_START =  VOID + BasReg.SINGLE_SPACE + VariableReg.NAME_OPTION1 + BasReg.OPEN_PAR;
    private static final String DEC_MIDDLE_OPTION1 = "(" + SUB_DECLARATION + ")";
    private static final String DEC_MIDDLE_OPTION2 = "(" + SUB_DECLARATION + BasReg.COMMA + ")";
    private static final String DEC_END = BasReg.CLOSE_PAR + BasReg.OPEN_BLOCK;

    private static final String FUNC_DEC = DEC_START + "(" + DEC_MIDDLE_OPTION1 + BasReg.ZERO_OR_ONE + "|" +
			"(" + DEC_MIDDLE_OPTION2 + BasReg.ONE_OR_MORE + DEC_MIDDLE_OPTION1 + ")" + ")"+ DEC_END;
	
	// Call structure
    private static final String SUB_CALL = "(" + VariableReg.VARIABLE_NAME + "|" + VariableReg.ARG_NAME + ")";

    private static final String CALL_START = VariableReg.NAME_OPTION1 + BasReg.OPEN_PAR;
    private static final String CALL_MIDDLE_OPTION1 = "(" + SUB_CALL + ")";
    private static final String CALL_MIDDLE_OPTION2 = "(" + SUB_CALL + BasReg.COMMA + ")";
    private static final String CALL_END = BasReg.CLOSE_PAR + BasReg.LINE_TERMINATOR;

    private static final String FUNC_CALL = CALL_START + "(" + CALL_MIDDLE_OPTION1 + BasReg.ZERO_OR_ONE + "|" +
			"(" + CALL_MIDDLE_OPTION2 + BasReg.ONE_OR_MORE + CALL_MIDDLE_OPTION1 + ")" + ")"+ CALL_END;

    /**
     * @return the regex which method calling line should fit to
     */
    public static String getFuncCallRegex (){
        return FUNC_CALL;
    }
    /**
     * @return the regex which method decleration line should fit to
     */
    public static String getFuncDecRegex(){
		return FUNC_DEC;
	}
	
}
