package oop.ex6.main.regexCommands;

/**
 * A class contains basic regex expressions, used in the other Regex classes in the package
 */

public class BasReg {

    static final String MAYBE_SINGLE_SPACE ="\\s?";
	static final String SINGLE_SPACE = " ";
	static final String MORE_THAN_SINGLE_SPACE = "\\s{2,}";

    static final String ANY_LENGTH_SPACE = "\\s*";

    static final String MAYBE_SOME_CHARS = "[\\w]*";
	static final String SOME_CHARS = "[\\w]+";
	static final String SINGLE_LETTER = "[a-zA-Z]";
	static final String ALL_CHARS_INC_SPACE = ".*";

	static final String UNDERSCORE = "_";
	static final String OPEN_BLOCK = "\\{";
	static final String COMMA = MAYBE_SINGLE_SPACE + "," + MAYBE_SINGLE_SPACE;
    static final String EQUAL = MAYBE_SINGLE_SPACE + "=" + MAYBE_SINGLE_SPACE;
	static final String LINE_TERMINATOR = MAYBE_SINGLE_SPACE + ";";
    static final String SINGLE_QUETE = "'";

    static final String OR_CON = MAYBE_SINGLE_SPACE + "\\|\\|" + MAYBE_SINGLE_SPACE;
    static final String AND_CON = MAYBE_SINGLE_SPACE + "&&" + MAYBE_SINGLE_SPACE;
    //TODO - notice the ANY_LENGTH_SPACE addition. can be more than one space!
	static final String OPEN_PAR = BasReg.ANY_LENGTH_SPACE + "\\(" + BasReg.ANY_LENGTH_SPACE;
	static final String CLOSE_PAR = BasReg.ANY_LENGTH_SPACE + "\\)" + BasReg.ANY_LENGTH_SPACE;

	static final String ZERO_OR_ONE = "?";
	static final String ZERO_OR_MORE = "*";
	static final String ONE_OR_MORE = "{1,}";

    /**
     * trims a given line and turns every space sequence into single space only
     * @param line
     * @return the given line after the trim process
     */
	public static String trimExpression (String line){
		line = line.trim();
		return line.replaceAll(MORE_THAN_SINGLE_SPACE, SINGLE_SPACE);
	}
}