package oop.ex6.main.regexCommands;

/**
 * A class contains variable regex expressions and methods used in order to determine if variable expression line such
 * as declaration or placement is right
 */

public class VariableReg {

	// Variable types
	static final String INT = "int", DOUBLE = "double", BOOLEAN = "boolean", CHAR = "char", STRING = "String";
	static final String FINAL = "final";
	static final String MAYBE_FINAL = "(" + FINAL + BasReg.SINGLE_SPACE + ")" + BasReg.ZERO_OR_ONE;
	
	static final String VARIABLE_TYPE = "(" + INT+"|"+DOUBLE+"|"+ BOOLEAN +"|"+CHAR+"|"	+STRING + ")";

   	// Variables name
    static final String NAME_OPTION1 = BasReg.SINGLE_LETTER + BasReg.MAYBE_SOME_CHARS;
    static final String NAME_OPTION2= BasReg.UNDERSCORE + BasReg.SOME_CHARS;
		
	static final String VARIABLE_NAME = "(" +NAME_OPTION1 + "|" + NAME_OPTION2+ ")";

    // arg types
    public static final String INT_ARG = "([-+]?\\d+)";
    public static final String STRING_ARG = "(\"(.)*\")";
    public static final String CHAR_ARG = BasReg.SINGLE_QUETE + "(.)" + BasReg.SINGLE_QUETE;
    public static final String DOUBLE_ARG =  "([-+]?\\d*\\.?\\d+|[-+]?\\d+\\.)";
    public static final String BOOLEAN_ARG = "([-+]?\\d*\\.?\\d+|[-+]?\\d+\\.|true|false)";

    public static final String ARG_NAME = "(" +INT_ARG + "|" + STRING_ARG + "|" + CHAR_ARG + "|" + DOUBLE_ARG + "|"
            + BOOLEAN_ARG + ")";

    // placement types (implicit or explicit)   implicit: such as int a;
    //                                          explicit: such as int a = 5;
    private static final String IMPLICIT_PLACEMENT = VARIABLE_NAME;
    private static final String EXPLICIT_PLACEMENT = VARIABLE_NAME + BasReg.EQUAL + "(" + ARG_NAME + "|" + BasReg.SOME_CHARS
            + ")";

    private static final String EXP_OR_IMP_PLACEMENT = "(" + EXPLICIT_PLACEMENT + "|" +	IMPLICIT_PLACEMENT + ")";

    // Placement
    private static final String VAR_CALL = EXPLICIT_PLACEMENT + BasReg.LINE_TERMINATOR;

	// Declaration
    private static final String DEC_START = MAYBE_FINAL + VARIABLE_TYPE + BasReg.SINGLE_SPACE;
    private static final String DEC_MIDDLE_OPTION1 = "(" + EXP_OR_IMP_PLACEMENT + BasReg.COMMA + ")";
    private static final String DEC_MIDDLE_OPTION2 = EXP_OR_IMP_PLACEMENT;
    private static final String DEC_END = BasReg.LINE_TERMINATOR;

    private static final String VAR_DEC = DEC_START + DEC_MIDDLE_OPTION1 + BasReg.ZERO_OR_MORE +
			DEC_MIDDLE_OPTION2 + DEC_END;

    /**
     * @return the regex which variable placement line should fit to
     */
	public static String getVarCallRegex (){
		return VAR_CALL;
	}

    /**
     * @return the regex which variable decleration line should fit to
     */
    public static String getVarDecRegex(){
		return VAR_DEC;
	}
}