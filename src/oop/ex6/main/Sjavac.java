package oop.ex6.main;

import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import oop.ex6.main.ast.ASTHandler;
import oop.ex6.main.ast_validator.MainValidator;
import oop.ex6.main.ast_validator.ValidationException;
import oop.ex6.main.command_line.TextLine;
import oop.ex6.main.command_line.TextLineFactory;
import oop.ex6.main.command_line.VarDecLine;
import oop.ex6.main.ast.ASTBuilder;
import oop.ex6.main.commands.Command;
import oop.ex6.main.regexCommands.BasReg;
import oop.ex6.main.utils.Logger;


public class Sjavac {

    private static ASTBuilder astBuilder = ASTBuilder.getInst();
    private static MainValidator validator = MainValidator.getInst();

	public static void main(String[] args) {



        initAll();

        Logger.printlnVV(validator.globNodes);
		String commandFile = args[0];


		FileReader fileReader = null;
		BufferedReader bufferedReader = null;

		LinkedList<String> lines = new LinkedList<String>();
		
		try{
			fileReader = new FileReader(commandFile);
			bufferedReader = new BufferedReader(fileReader);
			String line = null;


			while ((line = bufferedReader.readLine()) != null) {
				lines.add(line);
			}
		}
		catch  (FileNotFoundException e) 
		{
			Logger.println("File not found! " + args[0]);
            
            //RETURN SEQUENCE:
            System.out.print(2);
            return;
		}
		catch (IOException e) {
			// Return 2 for IOException
			Logger.println(e.toString());
            
            //RETURN SEQUENCE:
            System.out.print(2);
            return;
		}
		// THROWS TEXT_LINE_EXECPTION
		for (String line : lines){
            Logger.println(line);
			TextLine txtline = null;
            try {
                //TODO instead of returning lines! just push them to AST directly
                txtline = TextLineFactory.recognizeLine(line);
                // now line is recognized lets check validity:
                Logger.printlnVV(txtline);
            } catch (Exception e){
                 Logger.println(e.getMessage());
                
                 //RETURN SEQUENCE:
                System.out.print(1);
                return;
            }
            line = BasReg.trimExpression(line);
            String reg = txtline.getRegex();
//            Logger.printlnVV(reg + "|" + line);
//            Logger.printlnVV(line);
            if (line.matches(reg)) {
                Logger.printlnVV("VALIDATION PROCESS SUCCEEDED");
                if (txtline.isMultipleCommandLine()){
                    VarDecLine vdl = (VarDecLine) txtline;
                    for (Command cm : vdl.splitVarDecLine()){
                        ASTBuilder.getInst().newComamnd(cm);
                    }
                } else {
                     ASTBuilder.getInst().newComamnd(txtline.activateCommand());
                }
            } else {
                //TODO CODE 1 CLOSE
                Logger.printlnVV("INVALID LINE!");
                
                //RETURN SEQUENCE:
                System.out.print(1);
                return;
            }
		}
        try {
            ASTHandler astHandler= astBuilder.generateAST();
            astHandler.print();

            validator.validateAST(astHandler);
            System.out.print(0);
            return;
        } catch (ValidationException e){
            Logger.println(e.getMessage());
            
            //RETURN SEQUENCE:
            System.out.print(1);
            return;
        } catch (Exception other){
            Logger.println("other exception: " + other.getMessage());
            throw new RuntimeException("unhandled exception at Logic Validation stage!");
        }

//        return 0;
		
	}

    private static void initAll(){
        VarDecLine.initAll();
        astBuilder.initBuilder();
        validator.initValidator();
        Command.lineNum=0;

    }

}
