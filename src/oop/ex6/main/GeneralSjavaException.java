package oop.ex6.main;

/**
 * Created by dos884 on 6/21/15.
 */
public class GeneralSjavaException extends RuntimeException {
    public GeneralSjavaException() {
        super();
    }

    public GeneralSjavaException(String s) {
        super(s);
    }
}
