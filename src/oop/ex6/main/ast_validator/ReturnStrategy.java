package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTBuilder;
import oop.ex6.main.ast.ASTCommandNode;
import oop.ex6.main.ast.ASTFork;
import oop.ex6.main.ast.AbstractASTNode;
import oop.ex6.main.utils.Logger;

/**
 * Created by dos884 on 6/16/15.
 */
public class ReturnStrategy extends BackValidationStrategy {
    @Override
    public void invoke(ASTCommandNode node, boolean global) {
        //Climb up the scopes. use double pointer
        AbstractASTNode nextNode =  node.getParent(), thisNode =node;
        while (nextNode.getParent()!=null) {

            Logger.printlnVV(nextNode + " " + thisNode);
            thisNode=thisNode.getParent();
            nextNode = (ASTFork)nextNode.getParent();

        };

        if(((ASTFork)thisNode).getForkType()!= ASTBuilder.forkTypes.FUNCTION){
            throw new ValidationException("At: " + node.getCommand() +" got return statemet in: " +
                    node.getParent());
        }

    }

    private static ReturnStrategy inst = new ReturnStrategy();
    public static ReturnStrategy getInst(){return inst;}
}
