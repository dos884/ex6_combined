package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTBuilder;
import oop.ex6.main.ast.ASTCommandNode;
import oop.ex6.main.ast.ASTFork;
import oop.ex6.main.commands.Command;

/**
 * Created by dos884 on 6/18/15.
 */
public class EndScopeStrategy extends BackValidationStrategy {

    private EndScopeStrategy() {
        super();
        
    }

    private static EndScopeStrategy inst = new EndScopeStrategy();
    public static EndScopeStrategy getInst(){return inst;}

    @Override
    public void invoke(ASTCommandNode node, boolean global) {

        if(((ASTFork)node.getParent()).getForkType()== ASTBuilder.forkTypes.FUNCTION){
            if (((ASTCommandNode)node.prev(1)).getCommand().getCommandType()!= Command.commandTypes.RETURN) {
                throw new ValidationException("At: " + node.getCommand() + " got no return statemet in: " +
                        node.getParent());
            }
        }

    }


}
