package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTCommandNode;
import oop.ex6.main.ast.ASTHandler;
import oop.ex6.main.commands.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
* Created by dos884 on 6/15/15.
*/
public class FunctionCallStrategy extends BackValidationStrategy {


    private FunctionCallStrategy() {
        super();

    }

    private static FunctionCallStrategy inst = new FunctionCallStrategy();
    public static FunctionCallStrategy getInst(){return inst;}

    @Override
    public void invoke(ASTCommandNode node, boolean global) {
        FunCall call = (FunCall) node.getCommand();
        FuncDec funcDec = MainValidator.getInst().getHandler().findFuncDec(node.getCommand().getName());
        //no error
        if (funcDec==null) {
            throw new ValidationException("did not find matching function deceleration: " + call);
        }
        List<AssignmentArgument> args = call.getArgs();

        if (args==null || args.size() != funcDec.getVars().size()) {

            throw new ValidationException("at: " + node.getCommand() + " error with args " + args);
        }


        //we also create a list of variables to later match against the function declaration
        List<Variable> variableList = new ArrayList<Variable>();

        //do double iteration on Vars and Args
        Iterator<Variable> funcDecIter = funcDec.getVars().iterator();
        // Iterator for args
        Iterator<AssignmentArgument> argsIterator = args.iterator();


        while (funcDecIter.hasNext() && argsIterator.hasNext()) {
//        for (AssignmentArgument arg : args) {

            AssignmentArgument arg = argsIterator.next();
            Variable funcVar = funcDecIter.next();

            if (arg.isIsLiteral()){
                //create a literal-dummy
                variableList.add(new Variable(arg.getType()));
                continue;
            }
            //if not literal, search for it
            ASTCommandNode rhsNode = MainValidator.getInst().search(node, arg, false);

            //if var not found or uninited:
            if (rhsNode == null ||
                    !rhsNode.getCommand().getVar().isInitialized()) {
                throw new ValidationException("at: " + node.getCommand() + " error with rhs " + rhsNode +
                        " and given argument: "+arg);

            } else {
                variableList.add(rhsNode.getCommand().getVar());

            }
        }

        /*
            now check that the function calls actually match
         */

        if (!funcDec.matchVars(variableList)){
            throw new ValidationException("at: " +node.getCommand() + " vs: " + funcDec + " Arguments dont match!");
        }

        //no errors return
        return;
    }
}
