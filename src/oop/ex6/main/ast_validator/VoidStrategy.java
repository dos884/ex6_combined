package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTCommandNode;

/**
 * Created by dos884 on 6/20/15.
 */
public class VoidStrategy extends BackValidationStrategy {
    private VoidStrategy() {
        super();
    }

    private static VoidStrategy inst = new VoidStrategy();
    public static VoidStrategy getInst(){return inst;}

    @Override
    public void invoke(ASTCommandNode node, boolean global) {

    }
}
