package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTCommandNode;

/**
 * the strategy superclass. each command will use a different strategy to do validation.
 */
public abstract class BackValidationStrategy {

    MainValidator validator;

    BackValidationStrategy() {
        this.validator=MainValidator.getInst();
    }

    /**
     * invokes the strategy
     * @param node - node of invocation
     * @param global - is it a global node?
     */
    public abstract void invoke(ASTCommandNode node, boolean global);
}
