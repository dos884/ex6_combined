package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTCommandNode;
import oop.ex6.main.commands.AssignmentArgument;
import oop.ex6.main.commands.Cond;
import oop.ex6.main.commands.Variable;

import java.util.List;

/**
 * Created by dos884 on 6/17/15.
 */
public class ConditionalStrategy extends BackValidationStrategy {

    private static ConditionalStrategy inst = new ConditionalStrategy();
    public static ConditionalStrategy getInst(){return inst;}

    @Override
    public void invoke(ASTCommandNode node, boolean global) {

        Cond cond = (Cond)node.getCommand();

        List<AssignmentArgument> args = cond.getArgs();
        if (args == null || args.isEmpty()){
            return;
        }
        //a duumy variable to check for type matches. we treat it like it was an LHS in an assignment
        Variable lhsDummyBooleanVar = new Variable(false, Variable.valueTypes.BOOL,true,"dummy");
        for (AssignmentArgument arg : args)
        {
            //literal?
            if (arg.isIsLiteral()) {
                //literals do type match agaist BOOL
                Variable rhsDummyBooleanVar = new Variable(arg.getType());
                if (Variable.isTypeMatch(lhsDummyBooleanVar,rhsDummyBooleanVar)){
                    continue;
                } else {
                    throw new ValidationException("at: " + node.getCommand() + " type error with rhs " + arg);
                }

            }

            /* If not literal: need to see if:
                a. node is found
                b. types match
                c. the found variable is initialized
             */
            ASTCommandNode rhsNode = MainValidator.getInst().search(node,arg,false);

            if (rhsNode == null ||
                    !Variable.isTypeMatch(lhsDummyBooleanVar,rhsNode.getCommand().getVar())||
                    !rhsNode.getCommand().getVar().isInitialized()) {
                throw new ValidationException("at: " + node.getCommand() + " error with rhs [" + rhsNode +
                        "] and given argument: ["+arg+"]");
            }

        }

    }
}
