package oop.ex6.main.ast_validator;

import oop.ex6.main.GeneralSjavaException;

/**
 * Created by dos884 on 6/13/15.
 */
public class ValidationException extends GeneralSjavaException {

    public ValidationException(String message) {
        super(message);
    }
}
