package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTBuilder;
import oop.ex6.main.ast.ASTCommandNode;
import oop.ex6.main.ast.ASTFork;
import oop.ex6.main.ast.AbstractASTNode;
import oop.ex6.main.commands.Command;
import oop.ex6.main.commands.FuncVarDec;
import oop.ex6.main.commands.VarDec;
import oop.ex6.main.commands.Variable;

/**
* Created by dos884 on 6/15/15.
*/
public class VarDecStrategy extends BackValidationStrategy {
    private VarDecStrategy() {
        super();
    }

    private static VarDecStrategy inst = new VarDecStrategy();
    public static VarDecStrategy getInst(){return inst;}

    @Override
    public void invoke(ASTCommandNode node, boolean global) {
        VarDec startCommand = (VarDec)node.getCommand();

        Variable vardecVar = startCommand.getVar();
        //finals must be inited at decleration time:
        ASTFork parent = (ASTFork) node.getParent();


        if (startCommand.getCommandType()== Command.commandTypes.FUNC_VAR_DEC){
            startCommand.initVar();

        } else if (vardecVar.isFinal() && startCommand.getArg() == null ){
            throw new ValidationException("at: " + startCommand+ " is final and uninited!");
        }





        //if rhs of var dec isnt null -> check argument validity, i.e does it exist and do types match?
        if (startCommand.getArg() != null) {
            ValidationHelper.rhsToLhsAssignment(startCommand.getArg(), startCommand.getVar(), node, global);
        } // otherwise we dont to check anything

        //we want a var dec to check only against its current scope
        // var dec in any other scop will be overriden and it legal.
        while (node.prev(node.getDepth()-1) != null) {

            AbstractASTNode absNode = node.prev(node.getDepth()-1);
            node = (ASTCommandNode) absNode;
            //validate:

            if (startCommand.compare(node.getCommand())){
                throw new ValidationException("variable already declared!"+startCommand);
            }
        }
        return;
    }
}
