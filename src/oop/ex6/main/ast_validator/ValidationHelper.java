package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTCommandNode;
import oop.ex6.main.commands.AssignmentArgument;
import oop.ex6.main.commands.Variable;

/**
 * Created by dos884 on 6/19/15.
 */
public class ValidationHelper {

    /**
     * searches for RHS and checks if it matches the LHS Variable
     * @param rhs
     * @param lhs
     * @return
     */
    public static void rhsToLhsAssignment(AssignmentArgument rhs, Variable lhs, ASTCommandNode node, boolean global){

        ASTCommandNode rhsMatchingNode = null;


        Variable rhsVar =null;
        //if rhs literal make dummy variable
        if (rhs.isIsLiteral()) {
            rhsVar = new Variable(rhs.getType());
        //othrwise search for matching node
        } else {

            rhsMatchingNode = MainValidator.getInst().search(node,rhs,global);
            if (rhsMatchingNode == null){
                throw new ValidationException("at: " + node.getCommand().toString()+
                        " RHS node is not literal and couldnt be found:  " + rhs + " is " +  rhsMatchingNode);
            }else {
                rhsVar = rhsMatchingNode.getCommand().getVar();
                if (!rhsVar.isInitialized()){
                    throw new ValidationException("at: " + node.getCommand().toString()+
                            " RHS node is an uninitialized variable  " + rhs + " is " +  rhsMatchingNode);
                }
            }

        }

        //lhs var is given. we want to mathc agaist it

//        ASTCommandNode lhsMatchingNode = MainValidator.getInst().search(node,startCommand.getLHSArg(),global);

        Variable lhsVar = lhs;

        /*
            now check types
         */

        if (!Variable.isTypeMatch(lhsVar, rhsVar)) {
            throw new ValidationException("type dont match: " + lhsVar + " " + rhsVar);
        }



    }
}
