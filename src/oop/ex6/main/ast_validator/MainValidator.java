package oop.ex6.main.ast_validator;

import oop.ex6.main.utils.Logger;
import oop.ex6.main.ast.ASTCommandNode;
import oop.ex6.main.ast.ASTHandler;
import oop.ex6.main.ast.AbstractASTNode;
import oop.ex6.main.commands.AssignmentArgument;
import oop.ex6.main.commands.Command;
import oop.ex6.main.commands.VarDec;
import oop.ex6.main.commands.Variable;

import java.util.HashMap;
import java.util.List;

/**
 * the main validator singleton.
 */
public class MainValidator {

    private static MainValidator inst = new MainValidator();
    public static MainValidator getInst(){return inst;}

    public ASTHandler handler;

    public HashMap<String, ASTCommandNode> globNodes;

    private MainValidator(){
        initValidator();
    }

    public void initValidator(){
        globNodes =new HashMap<String, ASTCommandNode>();
    }

    private void pushGlobalNode(ASTCommandNode node){

        if(node.getCommand().getCommandType()== Command.commandTypes.VAR_DEC){
            Command c = (VarDec)node.getCommand();
            globNodes.put(c.getVar().getName(), node);
        }
    }

    /**
     * the main validation function
     * @param handler an ast handler.
     */
    public void validateAST(ASTHandler handler){
        this.handler = handler;

        checkGlobalScope();
        //check func decs
        checkFuncDecs();

        AbstractASTNode cur = AbstractASTNode.getFirstCommandNode(handler.getGlobalNode());
        while (cur.next()!=null){

            backValidate((ASTCommandNode)cur, false);
            cur=cur.next();

        }
    }

    ASTHandler getHandler(){
        return handler;
    }

    private void checkFuncDecs(){}

    /**
     * do back validation on the global vars list returned by the handler
     */
    private void checkGlobalScope(){

        List<AbstractASTNode> globals = handler.getGlobalNode().getLeaves();
        for (AbstractASTNode node: globals){
            backValidate((ASTCommandNode)node, true);

            //push the checked node to the global variable commands set
            pushGlobalNode(((ASTCommandNode) node));

        }
    }

    private void backValidate(ASTCommandNode node, boolean global){

        /*
            each command will have its own validation strategy:
         */
        node.getCommand().backValidate(node,global);



    }

    /**
     * searches for the variable declatation or use an returns the node where it occurs
     * (theres no need in searching for a literal, we already know it exists!
     * @param node
     * @param arg
     * @param global
     * @return
     */
    public ASTCommandNode search(ASTCommandNode node, AssignmentArgument arg, boolean global) {
        int stopDepth = global?0:1;
        ASTCommandNode varToMatch = null; // the variable we are searching for
        Variable searchKeyVar = node.getCommand().getVar();
        if (arg.isIsLiteral()){

            throw new RuntimeException("Dont search for Literals! " + arg);
        }
        //otherwise search for it!
        else {
            ASTCommandNode tempNode = node;
            while (tempNode.prev(stopDepth) != null) {

                tempNode = (ASTCommandNode) tempNode.prev(stopDepth);


                if (tempNode.getCommand().getCommandType()== Command.commandTypes.VAR_DEC ||
                        tempNode.getCommand().getCommandType()== Command.commandTypes.FUNC_VAR_DEC) {
                    //match against this nodes potential var
                    Variable matchedVar = tempNode.getCommand().getVar();
                    if (matchedVar != null && areMatching(arg, matchedVar)) {
                        //Found a match!

                        Logger.printlnVV("match!");

                        return tempNode;
                    }
                }
            }
            //check globs if notfound:


            if (globNodes.containsKey(arg.getName())) {
                return globNodes.get(arg.getName());

            }

            //if no match found anywhere. return null.
            return null;
        }
    }

    public boolean areMatching(AssignmentArgument arg, Variable var){
        return arg.getName().equals(var.getName());
    }


}
