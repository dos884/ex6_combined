package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTCommandNode;
import oop.ex6.main.commands.*;

/**
 * Created by dos884 on 6/15/15.
 */
public class VarAssignmentStrategy extends BackValidationStrategy {

    private VarAssignmentStrategy() {
        super();

    }
    private static VarAssignmentStrategy inst = new VarAssignmentStrategy();
    public static VarAssignmentStrategy getInst(){return inst;}


    @Override
    public void invoke(ASTCommandNode node, boolean global) {
        int stopDepth = global?0:1;
        VarCall startCommand = (VarCall)node.getCommand();

        /**
         * first look for the RHS argument.
         */
        AssignmentArgument arg = startCommand.getRHSArg();

        ASTCommandNode rhsMatchingNode = null;

        Variable rhsVar =null;
        if (arg.isIsLiteral()) {
            rhsVar = new Variable(arg.getType());
        } else {

            rhsMatchingNode = MainValidator.getInst().search(node,arg,global);
            if (rhsMatchingNode == null){
                throw new ValidationException("RHS node is not literal and couldnt be found:  " + arg + " is " +  rhsMatchingNode);
            }else {
                rhsVar = rhsMatchingNode.getCommand().getVar();
            }

        }
        /**
         * Now look for the LeftHS variable we are assigning to.
         */
        ASTCommandNode lhsMatchingNode = MainValidator.getInst().search(node,startCommand.getLHSArg(),global);

        Variable lhsVar = null;
        /*
            if found null in any of them error
         */
        if (lhsMatchingNode==null){
            throw new ValidationException("LHS var was not found!  " + startCommand.getLHSArg() + " with RHS var: " + rhsVar);
        } else {

            //check if lhs var is funcdecvardec and final


            lhsVar = lhsMatchingNode.getCommand().getVar();

            if (lhsMatchingNode.getCommand().getCommandType()== Command.commandTypes.FUNC_VAR_DEC && lhsVar.isFinal()){
                throw new ValidationException("trying to modify final function argument");
            }
        }

        /*
            now check types
         */

        try {
            if (!Variable.isTypeMatch(lhsVar, rhsVar)) {
                throw new ValidationException("type dont match: " + lhsVar + " " + rhsVar);
            }
        } catch (Exception ignore){
            System.out.printf("");
        }

        /*
            now check final and inited stuff
         */

        if (lhsVar.isFinal()){
            throw new ValidationException(startCommand+"var is final: " + lhsMatchingNode + " vs: " + rhsVar);
        }
        if (! lhsVar.isInitialized()){
            //need to still check maybe the decleration is exactly in the previous line, i.e
            // if the this Commant.prev = Declarion line AND Declaration + this Command have the same parent
            if (node.prev(0) == lhsMatchingNode && node.getParent() == lhsMatchingNode.getParent()){
                //then we are good. also, initialize this node and the lhs node

                lhsVar.setInitialized(true);
            } else {
                throw new ValidationException("var is uninited: " + lhsMatchingNode);
            }
        }



        // only after all test passed, return.
        return;
    }
}

