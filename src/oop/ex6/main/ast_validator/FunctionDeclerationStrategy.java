package oop.ex6.main.ast_validator;

import oop.ex6.main.ast.ASTCommandNode;

/**
* Created by dos884 on 6/15/15.
*/
public class FunctionDeclerationStrategy extends BackValidationStrategy {


    private static FunctionDeclerationStrategy inst = new FunctionDeclerationStrategy();
    public static FunctionDeclerationStrategy getInst(){return inst;}

    public FunctionDeclerationStrategy() {
        super();

    }

    @Override
    public void invoke(ASTCommandNode node, boolean global) {
        if (!global){
            invoke(node);
        }
        //if we are indeed checking gloably its more complicated then.
    }

    public void invoke(ASTCommandNode node) {


        if (node.getDepth()>1) {
            throw new ValidationException("got funcdec argument. Error.");
        }
    }
}
