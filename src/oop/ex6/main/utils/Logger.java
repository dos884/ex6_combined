package oop.ex6.main.utils;
import java.io.PrintWriter;

/**
 * A logging class for nicer debug. PLEASE DON'T GRADE THIS!!
 */
public class Logger {


    /**A LOGGING CLASS, PLEASE DON'T GRADE THIS!!!*/
    private static String log;
    private static PrintWriter writer;
    private static boolean failedToInitialize = false;
    // this will be the logging class. only objects of this type will be
    // monitored
    private static Class<? extends Object> targetClass;// = ChainedHashSet.class;

    /**A LOGGING CLASS, PLEASE DON'T GRADE THIS!!!*/
    // compilation will skip all debug blocks if verbose is false
    // since this var is final!
//    public static final boolean minimalVerbose = false;



    /***/
    public static final boolean verbose = false;
    public static final boolean veryVerbose = false;
    /***/



//    public static final boolean veryVerbose = false;
    static {
        if (verbose) {
            /**A LOGGING CLASS, PLEASE DON'T GRADE THIS!!!*/
            try {
                writer = new PrintWriter("log.txt", "UTF-8");
            }
            /**A LOGGING CLASS, PLEASE DON'T GRADE THIS!!!*/ catch (Exception e) {
                Logger.println("Failed to initialize Logger!");
                failedToInitialize = true;

            }
            /**A LOGGING CLASS, PLEASE DON'T GRADE THIS!!!*/
            // catch exits and close logging.
            Runtime.getRuntime().addShutdownHook(new Thread() {

                @Override
                public void run() {
                    flush();
                    writer.close();
                }

            });
        }

    }

    /**A LOGGING CLASS, PLEASE DON'T GRADE THIS!!!*/
    /**
     * this is a logging convenience function.
     * for debug purposes.
     * @param str a message prefix.
     * @param obj the object to log
     */
    public static void println(String str, Object obj){
        if (verbose && obj.getClass() == targetClass) {
                Logger.println(str + obj.toString());
        }

    }

    /**
     * println very verbose
     * @param obj
     */
    public static void printlnVV(Object obj){
        if (veryVerbose){
            println(obj);
        }
    }

    /**A LOGGING CLASS, PLEASE DON'T GRADE THIS!!!*/
    /**
     * println replacer
     * @param object toprint
     */
    public static void println(Object object){
        if (verbose) {
            System.out.println(object);
        }

    }

    /**A LOGGING CLASS, PLEASE DON'T GRADE THIS!!!*/
    private static void flush(){

        if (!failedToInitialize && verbose){

            System.out.print("flush.. "+log);
            writer.print(log);
            log="";
        }

    }
}
