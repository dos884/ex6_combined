package oop.ex6.main.command_line;

import oop.ex6.main.commands.Command;
import oop.ex6.main.commands.VoidCommand;
/**
 * a class specified for the void line, means empty line/ comment line, contains its corresponding regex and
 * actions needed to be performed on line in order to send a Command to constructor
 */
public class VoidLine extends SimpleTextLine {

    private static final String  EVERY_CHAR = ".*";
	@Override
	public String getRegex() {
		//todo comment regex! need to check this!
        return EVERY_CHAR;
	}

	@Override
	public Command activateCommand() {

		return new VoidCommand();
	}

}
