package oop.ex6.main.command_line;

import oop.ex6.main.commands.Command;
import oop.ex6.main.commands.EndScope;
import oop.ex6.main.regexCommands.SimpleLineReg;
/**
 * a class specified for the end scope line, contains its corresponding regex and actions needed to be performed on line
 * in order to send a Command to constructor
 */
public class EndScopeLine extends SimpleTextLine {

	@Override
	public String getRegex() {
		return SimpleLineReg.getEndScopeRegex() ;
	}

	@Override
	public Command activateCommand() {

		return new EndScope();
		
	}

}
