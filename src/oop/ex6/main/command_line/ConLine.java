package oop.ex6.main.command_line;

import oop.ex6.main.commands.AssignmentArgument;
import oop.ex6.main.commands.Command;
import oop.ex6.main.commands.Cond;
import oop.ex6.main.regexCommands.BasReg;
import java.util.List;
import java.util.ArrayList;

/**
 * a class specified for the condition line, contains its corresponding regex and actions needed to be performed on line
 * in order to send a Command to constructor
 */
public class ConLine extends ComplicatedTextLine {
    /**
     * constructor for condition line
     * @param line
     */
	public ConLine(String line) {
		super(line);
	}

	@Override
	public String getRegex() {
		
		return oop.ex6.main.regexCommands.ConditionReg.getConReg();
	}

	@Override
	public Command activateCommand() {
		
		line = ArrangeExpression(line);
        return new Cond (isolateLineIntoArgs());
	}

    @Override
    protected String ArrangeExpression(String line) {

        line = LineHelper.omitSingleKeyWord(line);
        line = line.replace(LineHelper.OPEN_PAR, LineHelper.SINGLE_SPACE);
        line = line.replace(LineHelper.CLOSE_PAR,LineHelper.SINGLE_SPACE);
        line = LineHelper.omitSuffix(line,LineHelper.OPEN_BLOCK);
        line = line.replaceAll(LineHelper.OR, LineHelper.COMMA);
        line = line.replaceAll(LineHelper.AND, LineHelper.COMMA);
        line = BasReg.trimExpression(line);
        return line;
    }


    /*
        splits a string into words
     */
    private static String[] isolateVar (String line){
        String allVars[] = line.split(LineHelper.COMMA);
        allVars[0] = allVars[0].trim();
        return allVars;
    }

    /*
        isolates a line into arguments
     */
    private   List<AssignmentArgument> isolateLineIntoArgs() {
        List<AssignmentArgument> argList = new ArrayList<AssignmentArgument>();
        String[] allVars = isolateVar (line);
        if (!allVars[0].isEmpty()) {
            for (String singleCell : allVars) {
                AssignmentArgument arg = LineHelper.stringIntoArg(singleCell);
                argList.add(arg);
            }
        }
        return argList;
    }


}
