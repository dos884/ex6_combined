package oop.ex6.main.command_line;

import oop.ex6.main.commands.Command;
import oop.ex6.main.commands.ReturnStatement;
import oop.ex6.main.regexCommands.SimpleLineReg;

/**
 * a class specified for the return line, contains its corresponding regex and actions needed to be performed on line
 * in order to send a Command to constructor
 */
public class ReturnLine extends SimpleTextLine {

	@Override
	public String getRegex() {
		return SimpleLineReg.getReturnRegex() ;
	}


	@Override
	public Command activateCommand() {
		
		return new ReturnStatement();
		
	}

}
