package oop.ex6.main.command_line;

import oop.ex6.main.commands.*;
import oop.ex6.main.regexCommands.*;

import java.util.ArrayList;
import java.util.List;
/**
 * a class specified for the varaible declaration line, contains its corresponding regex and actions needed to be
 * performed on line in order to send a Command to constructor
 */
public class VarDecLine extends ComplicatedTextLine{
    /**
     * constructor for varaible declaration line
     * @param line
     */
	public VarDecLine(String line) {
		super(line);
	}

	public static boolean isFinal = false;
	private static Variable.valueTypes type;

 	@Override
	public String getRegex() {
		return VariableReg.getVarDecRegex();
	}


    @Override
    public boolean isMultipleCommandLine() {
        return true;
    }


	@Override
	public Command activateCommand() {

		line = ArrangeExpression(line);
		String[] allVars = isolateVar (line);

		for (String singleCell : allVars){
			return sendNewCommand(singleCell);
		}
		return null;
	}


	@Override
	protected String ArrangeExpression(String line) {

		String firstWord = LineHelper.getFirstWord(line);
		line = updateFinal (firstWord, line);
		type = LineHelper.stringintoEnum(LineHelper.getFirstWord(line));
        line = LineHelper.omitSingleWord(line);
        line = LineHelper.omitSuffix (line, LineHelper.LINE_TERMINATOR);
		return line;
	}

    // checks if the word final appears in line and updates final values in this case
	private static String updateFinal (String firstWord, String line){
		if (firstWord.equals("final")){
			isFinal = true;
            line = LineHelper.omitSingleWord(line);
        }
		return line;
	}

	// splits the given String around Commas and returns only variables singles/pair
	private static String[] isolateVar (String line){

		line = line.replaceAll(LineHelper.EQUAL,LineHelper.SINGLE_SPACE);
		String[] seperateVar = line.split(LineHelper.COMMA);
		return seperateVar;
	}

	// returns true iff the expression is a placement (of value/another variable into created variable)
	// i.e  a = b
	private static boolean isPlacement (String expression){
		if (expression.contains(LineHelper.SINGLE_SPACE))
			return true;
		return false;
	}

    // creates required objectes and sends the new command
	private static Command sendNewCommand (String singleCell){
        singleCell = BasReg.trimExpression(singleCell);
        String name = LineHelper.getFirstWord(singleCell);

		if (isPlacement(singleCell)){
            singleCell = LineHelper.omitSingleWord(singleCell);
            AssignmentArgument arg = LineHelper.stringIntoArg(singleCell);

			return new VarDec(isFinal , type,  name, arg);
		}
		else{
			return new VarDec(isFinal , type,  name);
		}
	}

    /**
     *  initiazlies static common values before anaylizing line
     */
    public static void initAll(){
        isFinal = false;
        type = null;
    }

    /**
     *
     * @return a list of commands fits to all variables in the line
     */
    public List<Command> splitVarDecLine(){
        List<Command> list = new ArrayList<Command>();

        line = ArrangeExpression(line);
        String[] allVars = isolateVar (line);

        for (String singleCell : allVars){
            list.add(sendNewCommand(singleCell));
        }
        isFinal =false;
        return list;
    }

}		

