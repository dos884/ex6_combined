package oop.ex6.main.command_line;

import oop.ex6.main.regexCommands.BasReg;

/**
 * A factory class used for initial recognition of the given line, in order to compare
 * it with the correspond regex expression and activates its constructor in case regex fits
 */

public class TextLineFactory {

    private static final String COMMENT_SIGN = "//", END_SCOPE_SIGN = "}", LINE_TERMINATOR_SIGN = ";",
    RETURN_SIGN = "return", OPEN_PAR_SIGN = "(", INT_SIGN = "int", BOOLEAN_SIGN = "boolean", DOUBLE_SIGN = "double",
    CHAR_SIGN = "char", STRING_SIGN = "String",OPEN_SCOP_SIGN = "{", VOID_SIGN = "void";

	public static TextLine recognizeLine (String line) throws TextLineException {

        line = BasReg.trimExpression(line);

        if (line.isEmpty() || line.startsWith(COMMENT_SIGN)) {
            return new VoidLine();
        }
        if (line.endsWith(END_SCOPE_SIGN)) {
            return new EndScopeLine();
        }
        if (line.endsWith(LINE_TERMINATOR_SIGN)) {

            if (line.startsWith(RETURN_SIGN)) {
                return new ReturnLine();
            }
            if (line.contains(OPEN_PAR_SIGN)) {
                return new FuncCallLine(line);
            }
            if (line.contains(INT_SIGN) || line.contains(BOOLEAN_SIGN) || line.contains(DOUBLE_SIGN) ||
                    line.contains(CHAR_SIGN) || line.contains(STRING_SIGN)) {
                return new VarDecLine(line);
            }
            return new VarCallLine(line);
        }

        if (line.endsWith(OPEN_SCOP_SIGN)) {

            if (line.startsWith(VOID_SIGN)) {
                return new FuncDecLine(line);
            } else {
                return new ConLine(line);
            }
        }

        throw new TextLineException();
    }
}





