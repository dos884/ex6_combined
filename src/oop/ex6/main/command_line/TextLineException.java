package oop.ex6.main.command_line;
/**
 * An exception thrown by TextLineFactory in case there is line which has not even intial recognition
 * @author black_knight
 *
 */
public class TextLineException extends Exception{

	private static final long serialVersionUID = 1L;

}
