package oop.ex6.main.command_line;

import oop.ex6.main.commands.AssignmentArgument;
import oop.ex6.main.commands.Variable;
import oop.ex6.main.regexCommands.VariableReg;
import oop.ex6.main.regexCommands.RegexCompartor;

/**
 * A helper class for common actions being performed on the given line, in order to seperate parts from the expression
 * and arrange it, for creating the corresponding objects
 */

public class LineHelper {

	private static final int NOT_EXIST = -1, LINE_START = 0;
    static final int FIRST_WORD = 1;
	static final String SINGLE_SPACE = " ", OPEN_PAR = "(", CLOSE_PAR = ")", EQUAL = "=", 
			LINE_TERMINATOR = ";", COMMA = ",", OPEN_BLOCK = "{" , OR ="[|]{2}" , AND = "[&]{2}",
        FIRST_WORD_BEFORE_PAR = "[a-zA-Z][\\w]*\\s*\\(";

    /**
     * returns the first word in the given line
     * @param line
     * @return the first word in the given line
     */
	static String getFirstWord (String line ){
		if (line.indexOf(SINGLE_SPACE) != NOT_EXIST){
			return line.substring(LINE_START, line.indexOf(SINGLE_SPACE));
		}
		else
			return line; 
	}
	

    /**
     * Omits all the expression after a given suffix, including this suffix
     * @param line
     * @param suffix
     * @return line after specified changes
     */
	static String omitSuffix (String line, String suffix){
		return line.substring(LINE_START, line.indexOf(suffix));
	}
    /**
     * Omits all the expression before a given preffix
     * @param line
     * @param prefix
     * @return line after specified changes
     */
    static String omitPrefix (String line, String prefix){
		return line.substring(line.indexOf(prefix) + 1);
	}

    /**
     * checks if some arg is a literal or a variable name
     * @param someArg
     * @return true iff the arg is a literal
     */
    static boolean isLiteral(String someArg){
		return (RegexCompartor.ValidateLine(someArg,VariableReg.ARG_NAME)? true : false);
	}

    /**
     * turns a string into corresponding enum
     * @param str
     * @return enum
     */
	static Variable.valueTypes stringintoEnum(String str){

        if (str.equals("int")) {
            return Variable.valueTypes.INT;
        } else if (str.equals("boolean")) {
            return Variable.valueTypes.BOOL;
        } else if (str.equals("char")) {
            return Variable.valueTypes.CHAR;
        } else if (str.equals("double")) {
            return Variable.valueTypes.DOUBLE;
        } else if (str.equals("String")) {
            return Variable.valueTypes.STRING;
        } else {
            return Variable.valueTypes.UNKNOWN;
        }
	}

    /**
     * turns a string into corresponding arg, after indentifing if it is a literal or not
     * @param str
     * @return arg
     */
	static AssignmentArgument stringIntoArg (String str){
		
		str = str.trim();
		if (isLiteral(str)){
            Variable.valueTypes type = identifyArgType (str);
			return new AssignmentArgument(str, type);
		}
		else{
			return new AssignmentArgument(str);
		}
	}	

    // omits a single word from the beggining of the line
	static String omitSingleWord (String line){
		return line.substring(line.indexOf(" ") + 1, line.length());
	}

    /**
     * omits a single key word from all the expression before par
     * @param line
     * @return line after changes
     */
    public static String omitSingleKeyWord(String line){
        return line.replaceAll(FIRST_WORD_BEFORE_PAR,OPEN_PAR);
    }

    // returns an enum according to the given argument
    private static Variable.valueTypes identifyArgType (String arg){
        if (arg.matches(VariableReg.INT_ARG))
            return Variable.valueTypes.INT;
        if (arg.matches(VariableReg.DOUBLE_ARG))
            return Variable.valueTypes.DOUBLE;
        if (arg.matches(VariableReg.CHAR_ARG))
            return Variable.valueTypes.CHAR;
        if (arg.matches(VariableReg.STRING_ARG))
            return Variable.valueTypes.STRING;
        if (arg.matches(VariableReg.BOOLEAN_ARG))
            return Variable.valueTypes.BOOL;
        return Variable.valueTypes.UNKNOWN;
    }

}