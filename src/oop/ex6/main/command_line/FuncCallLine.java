package oop.ex6.main.command_line;

import oop.ex6.main.commands.AssignmentArgument;
import oop.ex6.main.commands.Command;

import oop.ex6.main.commands.FunCall;
import oop.ex6.main.regexCommands.MethodReg;
import java.util.List;
import java.util.ArrayList;
/**
 * a class specified for the function calling line, contains its corresponding regex and actions needed to be performed
 * on line in order to send a Command to constructor
 */
public class FuncCallLine extends ComplicatedTextLine{
    /**
     * constructor for function calling line
     * @param line
     */
  	public FuncCallLine(String line) {
		super(line);
	}

	@Override
	public String getRegex() {
		return MethodReg.getFuncCallRegex();
	}

	@Override
	public Command activateCommand() {

		line = ArrangeExpression(line);
		String name = LineHelper.getFirstWord(line);
		line = LineHelper.omitSingleWord( line);

		List<AssignmentArgument> argList = new ArrayList<AssignmentArgument>();
		String[] allVars = isolateVar (line);

			if (!allVars[0].isEmpty()) {
         for (String singleCell : allVars) {
                AssignmentArgument arg = LineHelper.stringIntoArg(singleCell);
                argList.add(arg);
            }
        }

        return new FunCall(name,argList);
	}

	@Override
	protected String ArrangeExpression(String line) {

		line = line.replace(LineHelper.OPEN_PAR, LineHelper.SINGLE_SPACE);
		line = line.replace(LineHelper.CLOSE_PAR,LineHelper.SINGLE_SPACE);
		line = LineHelper.omitSuffix(line,LineHelper.LINE_TERMINATOR);
		return line;
	}

    // isolates the variables and placing them in an string array
	private static String[] isolateVar (String line){
		String allVars[] = line.split(LineHelper.COMMA);
        allVars[0] = allVars[0].trim();
        return allVars;
	}
}
