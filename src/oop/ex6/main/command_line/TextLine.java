package oop.ex6.main.command_line;

import oop.ex6.main.commands.Command;

/**
 * A super class for all types of line texts available. inherits directly to the simpleLine super class and
 * ComplicatedLIne super class
 */
public abstract class TextLine {
    /**
     * returns the correspond regex for this line type
     * @return a String with the correspond regex for this line type
     */
	public abstract String getRegex ();

    /**
     * some lines have multiple commands in them and we need to check which
     * type is it. e.g: "int a = 5, b, c, d = 6;
     * @return does line contain multiple commands per line
     */
    public boolean isMultipleCommandLine(){
        return false;
    }

    /**
     * turns the line, which is in a string format, into objects and sending it as a command to the corresponding
     * constructors
     * @return a Command which fits to the corresponding constructor of this line objects
     */
	public abstract Command activateCommand();


}