package oop.ex6.main.command_line;

import oop.ex6.main.commands.Command;
import oop.ex6.main.commands.FuncDec;
import oop.ex6.main.commands.Variable;
import oop.ex6.main.regexCommands.BasReg;
import oop.ex6.main.regexCommands.MethodReg;
import java.util.List;
import java.util.ArrayList;
/**
 * a class specified for the function declaration line, contains its corresponding regex and actions needed to be
 * performed on line in order to send a Command to constructor
 */
public class FuncDecLine extends ComplicatedTextLine {

    private static final int FIRST_CELL = 0;

    /**
     * constructor for function declaration line
     * @param line
     */
	public FuncDecLine(String line) {
		super(line);
	}

	@Override
	public String getRegex() {
		return MethodReg.getFuncDecRegex();
	}

	@Override
	public Command activateCommand() {

		line = ArrangeExpression(line);
		String name = LineHelper.getFirstWord(line);
        line = LineHelper.omitSingleWord(line);

		List<Variable> allVars = isolateVar (line);
        return new FuncDec(name, allVars);
	}

	@Override
	protected String ArrangeExpression(String line) {
        line = LineHelper.omitSingleWord(line);
		line = line.replace(LineHelper.OPEN_PAR, LineHelper.SINGLE_SPACE);
		line = line.replace(LineHelper.CLOSE_PAR,LineHelper.SINGLE_SPACE);
		line = LineHelper.omitSuffix(line,LineHelper.OPEN_BLOCK);
		return line;                                                                                
	}

    /**
     * orgnizes the variables in the given string line and returns a list of Variables objects
     * @param line
     * @return vars - List of Variables
     */
	private static List<Variable> isolateVar (String line){

        line = line.trim();
		String[] seperateVar = line.split(LineHelper.COMMA);

		List<Variable> vars = new ArrayList<Variable>();
        if (!seperateVar[FIRST_CELL].isEmpty()) {
            for (String singleCell : seperateVar) {
                vars.add(stringIntoVar(singleCell));
            }
        }
    	return vars;
	}

    // turns a given single variable string into an variable object
	private static Variable stringIntoVar (String singleCell){

        singleCell = BasReg.trimExpression(singleCell);
        boolean isFinal = false;
		if ((LineHelper.getFirstWord(singleCell)).equals("final")){
			isFinal = true;
            singleCell = LineHelper.omitSingleWord(singleCell);
		}

		Variable.valueTypes type  = LineHelper.stringintoEnum(LineHelper.getFirstWord(singleCell));
		singleCell = LineHelper.omitSingleWord( singleCell);
		return new Variable(isFinal, type, false, singleCell);
	}
}