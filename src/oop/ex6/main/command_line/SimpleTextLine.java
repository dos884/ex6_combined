package oop.ex6.main.command_line;

/**
 *  an abstract super class for: VoidLine, ReturnLine, EndScopeLine, which are all class which required no data from
 *  the given line itself.
 *  uses as a marker class, only for order purposes
 */
public abstract class SimpleTextLine extends TextLine {

}
