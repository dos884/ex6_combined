
package oop.ex6.main.command_line;

/**
 * a super class for all complicated lines, such as:
 * VarCallLine, VarDecLine, FuncCallLine, FuncDecLine, ConLine
 */
public abstract class ComplicatedTextLine extends TextLine {
	
	protected String line;

    /**
     * a constructor which contains the given line, in order to use by inheriting classes
     * @param line
     */
	public ComplicatedTextLine(String line) {
		this.line = line;
	}

    /**
     * a method for the initial actions being perfomed on the line in order to isolate the only the expressions of
     * the required data, e.g. variables
     * @param line
     * @return the line after required expressions omitting actions
     */
	abstract protected String ArrangeExpression(String line);
}
