package oop.ex6.main.command_line;

import oop.ex6.main.commands.AssignmentArgument;
import oop.ex6.main.commands.Command;

import oop.ex6.main.commands.VarCall;
import oop.ex6.main.regexCommands.BasReg;
import oop.ex6.main.regexCommands.VariableReg;

/**
 * a class specified for the varibale placement line, contains its corresponding regex and actions needed to be
 * performed on line in order to send a Command to constructor
 */
public class VarCallLine extends ComplicatedTextLine {
    /**
     * constructor for varaible calling line
     * @param line
     */
	public VarCallLine(String line) {
		super(line);
	}

	@Override
	public String getRegex() {
		return VariableReg.getVarCallRegex();
	}

	@Override
	public Command activateCommand() {

		line = ArrangeExpression(line);
		return sendNewCommand(line);
	}
	
	@Override
	protected String ArrangeExpression(String line) {
		
		line = LineHelper.omitSuffix (line,LineHelper.LINE_TERMINATOR);
		line = line.replace(LineHelper.EQUAL, LineHelper.SINGLE_SPACE);
		line = BasReg.trimExpression(line);
		return line;
	}
	// creates required objectes and sends the new command
	private static Command sendNewCommand (String line){

		String name = LineHelper.getFirstWord(line);
		line = LineHelper.omitSingleWord( line);
		AssignmentArgument arg = LineHelper.stringIntoArg(line) ;
        return new VarCall(name,arg);

	}
}
